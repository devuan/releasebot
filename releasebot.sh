#!/bin/bash

function error {
	echo "$1"
	exit 0
}

{
	flock -n 9 \
		|| error 'Can not get lock'

	/home/releasebot/devuan-releasebot/releasebot.py \
		-c /home/releasebot/devuan-releasebot/config.deploy \
	| grep -v '^[[][]]$'
} 9<$0 2>&1 | awk '{print strftime("[%Y-%m-%d %H:%M:%S]"),$0}' >> /home/releasebot/work.log
