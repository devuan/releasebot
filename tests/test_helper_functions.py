import unittest
import typing

from tests.lib import ConfigMockup


class TestHelperFunctions(unittest.TestCase):
    def _list_helper_test_case(
        self, value: str, result: typing.Iterable[str], separator: str = ","
    ):
        """
        Helper function to test _list_helper
        """
        from releasebot import _list_helper

        self.assertEqual(
            result,
            _list_helper(
                ConfigMockup({"section": {"item": value}}), "section", "item", separator
            ),
        )

    def test_comma_separated(self):
        self._list_helper_test_case("a,b,c", ["a", "b", "c"])

    def test_dot_separated(self):
        self._list_helper_test_case("a.b.c.d", ["a", "b", "c", "d"], ".")

    def test_comma_separated_spaces(self):
        self._list_helper_test_case("a  , b   , c", ["a", "b", "c"])

    def test_dot_separated_spaces(self):
        self._list_helper_test_case(
            "a   .  b    .   c  .     d", ["a", "b", "c", "d"], "."
        )
