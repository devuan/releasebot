import attr

from releasebot import GiteaRepoStore, JenkinsJobStore


class ConfigMockup(object):
    def __init__(self, data=dict()):
        self.data = data

    def get(self, section, item, fallback=None):
        return self.data.get(section, dict()).get(item, fallback)


@attr.s
class ArgsMockup(object):
    debug = attr.ib(default=False, type=bool)
    dryrun = attr.ib(default=False, type=bool)


class ResponseMockup(object):
    def __init__(self, response=dict(), ok=True):
        self.response = response
        self.ok = ok

    def json(self):
        return self.response


class GiteaRepoStoreMockup(GiteaRepoStore):
    def __init__(self, config, args, responses=dict()):
        super().__init__(config, args)
        self.responses = responses
        self.messages = []
        self.resolved_tasks = []

    def _request(
        self, method: str, url: str, params: dict = dict(), json: dict = dict()
    ):
        if not params and json:
            params = json
        # Treat notifications specially to keep track of them
        if url.endswith("/comments"):
            self.messages.append((url, json["body"]))
            return ResponseMockup()
        # Treat resolved tasks specially to keep track of them
        if method == "patch" and "/issues/" in url and json.get("state") == "closed":
            self.resolved_tasks.append(url)
            return ResponseMockup()
        return (
            self.responses.get(method, dict())
            .get(url, dict())
            .get(repr(params), ResponseMockup())
        )


class JenkinsAPIMockup(object):
    def __init__(self):
        self.data = dict()

    def build_job(self, job_name: str, data: dict):
        job_info = self.get_job_info(job_name)
        job_info["builds"].append(data)
        self.data[job_name]["nextBuildNumber"] += 1

    def get_job_info(self, job_name: str):
        if job_name not in self.data:
            self.data[job_name] = {
                "url": "/test-builds/",
                "builds": [],
                "nextBuildNumber": 10,
            }
        return self.data[job_name].copy()


class JenkinsJobStoreMockup(JenkinsJobStore):
    def _connect_jenkins(self):
        self.jk = JenkinsAPIMockup()
