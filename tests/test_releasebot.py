import unittest
import typing
import attr

from tests.lib import (
    GiteaRepoStoreMockup,
    ArgsMockup,
    ConfigMockup,
    ResponseMockup,
    JenkinsJobStoreMockup,
)

from releasebot import (
    ReleaseBot,
    ReleaseBotTask,
    ReleaseBotCommand,
    ReleaseBotRequester,
)

# This provides the testing model that we'll provide to our classes
# If you want to add test-cases, make sure you edit responses to match
# this description as well.
# expected_messages and expected_builds are used in the TestReleaseBot class
# to check proper functioning of ReleaseBot.
tested_open_issues = [
    {
        "number": 10,
        "title": "Build",
        "descr": "Not 'pending': not assigned to releasebot",
        "repository": {"name": "goodowner", "owner": "devuan",},
        "labels": [],
        "assignee": {"username": "koala",},
    },
    {
        "number": 11,
        "title": "Build",
        "descr": "Not 'pending': bad owner",
        "repository": {"name": "badstuff", "owner": "badone",},
        "labels": [],
        "assignee": {"username": "releasebot",},
    },
    {
        "number": 20,
        "title": "Build",
        "descr": "Is 'pending': but should have no permissions",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "unstable"}],
        "assignee": {"username": "releasebot",},
        "user": {"username": "koala"},
        "expected_messages": ["don't have permissions"],
    },
    {
        "number": 21,
        "title": "Build",
        "descr": "Is 'pending': should be collaborator. Has no labels.",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [],
        "assignee": {"username": "releasebot",},
        "user": {"username": "colab"},
        "expected_messages": ["assign some labels"],
    },
    {
        "number": 22,
        "title": "Build",
        "descr": "Is 'pending': should be member. Has no labels.",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [],
        "assignee": {"username": "releasebot",},
        "user": {"username": "dev1dev"},
        "expected_messages": ["assign some labels"],
    },
    {
        "number": 23,
        "title": "Build",
        "descr": "Is 'pending': should be admin. Has no labels.",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [],
        "assignee": {"username": "releasebot"},
        "user": {"username": "toor", "is_admin": True},
        "expected_messages": ["assign some labels"],
    },
    {
        "number": 24,
        "title": "Build",
        "descr": "Is 'pending': should be collaborator.",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "beowulf-proposed"}],
        "assignee": {"username": "releasebot",},
        "user": {"username": "colab"},
        "expected_messages": ["don't have permissions"],
    },
    {
        "number": 25,
        "title": "Build",
        "descr": "Is 'pending': should be member.",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "ascii-proposed"}],
        "assignee": {"username": "releasebot",},
        "user": {"username": "dev1dev"},
        "expected_messages": ["don't have permissions"],
    },
    {
        "number": 26,
        "title": "Build",
        "descr": "Don't build directly for stable suites",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "beowulf-security"}],
        "assignee": {"username": "releasebot"},
        "user": {"username": "toor", "is_admin": True},
        "expected_messages": ["Ignoring", "Finished queueing"],
    },
    {
        "number": 27,
        "title": "Build",
        "descr": "Is 'pending': admin build restricted.",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "beowulf-proposed-updates"}],
        "assignee": {"username": "releasebot"},
        "user": {"username": "toor", "is_admin": True},
        "expected_messages": ["Triggered", "Finished queueing"],
        "expected_builds": [
            {"codename": "beowulf-proposed-updates", "srcpackage": "stuff"}
        ],
    },
    {
        "number": 28,
        "title": "Build",
        "descr": "Ignore unknown labels",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "private"}],
        "assignee": {"username": "releasebot"},
        "user": {"username": "toor", "is_admin": True},
        "expected_messages": ["Ignoring", "Finished queueing"],
    },
    {
        "number": 29,
        "title": "Build",
        "descr": "Ignore unknown labels, but action relevant",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "private"}, {"name": "beowulf-proposed"}],
        "assignee": {"username": "releasebot"},
        "user": {"username": "toor", "is_admin": True},
        "expected_messages": ["Triggered", "Ignoring", "Finished queueing"],
        "expected_builds": [{"codename": "beowulf-proposed", "srcpackage": "stuff"}],
    },
    {
        "number": 30,
        "title": "Build",
        "descr": "Is 'pending', stuff collaborator",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "unstable"}],
        "assignee": {"username": "releasebot"},
        "user": {"username": "colab"},
        "expected_messages": ["Triggered", "Finished queueing"],
        "expected_builds": [{"codename": "unstable", "srcpackage": "stuff"}],
    },
    {
        "number": 31,
        "title": "Build",
        "descr": "Is 'pending', packages group member",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "experimental"}],
        "assignee": {"username": "releasebot"},
        "user": {"username": "dev1dev"},
        "expected_messages": ["Triggered", "Finished queueing"],
        "expected_builds": [{"codename": "experimental", "srcpackage": "stuff"}],
    },
    {
        "number": 32,
        "title": "Silly",
        "descr": "Unrecognised command",
        "repository": {"name": "stuff", "owner": "devuan",},
        "labels": [{"name": "unstable"}],
        "assignee": {"username": "releasebot",},
        "user": {"username": "toor", "is_admin": True},
        "expected_messages": ["Command not supported"],
    },
]


def expected_pending_tasks(repo_store):
    return [
        ReleaseBotTask(
            id="devuan/stuff/issues/20",
            command=ReleaseBotCommand(
                name="build", package="stuff", labels=set(["unstable"])
            ),
            requester=ReleaseBotRequester(name="koala", permissions=[]),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/21",
            command=ReleaseBotCommand(name="build", package="stuff", labels=set()),
            requester=ReleaseBotRequester(
                name="colab", permissions=["collaborator/stuff"]
            ),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/22",
            command=ReleaseBotCommand(name="build", package="stuff", labels=set()),
            requester=ReleaseBotRequester(name="dev1dev", permissions=["member"]),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/23",
            command=ReleaseBotCommand(name="build", package="stuff", labels=set()),
            requester=ReleaseBotRequester(name="toor", is_admin=True, permissions=[]),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/24",
            command=ReleaseBotCommand(
                name="build", package="stuff", labels=set(["beowulf-proposed"])
            ),
            requester=ReleaseBotRequester(
                name="colab", permissions=["collaborator/stuff"]
            ),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/25",
            command=ReleaseBotCommand(
                name="build", package="stuff", labels=set(["ascii-proposed"])
            ),
            requester=ReleaseBotRequester(name="dev1dev", permissions=["member"]),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/26",
            command=ReleaseBotCommand(
                name="build", package="stuff", labels=set(["beowulf-security"])
            ),
            requester=ReleaseBotRequester(name="toor", is_admin=True, permissions=[]),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/27",
            command=ReleaseBotCommand(
                name="build", package="stuff", labels=set(["beowulf-proposed-updates"])
            ),
            requester=ReleaseBotRequester(name="toor", is_admin=True, permissions=[]),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/28",
            command=ReleaseBotCommand(
                name="build", package="stuff", labels=set(["private"])
            ),
            requester=ReleaseBotRequester(name="toor", is_admin=True, permissions=[]),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/29",
            command=ReleaseBotCommand(
                name="build",
                package="stuff",
                labels=set(["private", "beowulf-proposed"]),
            ),
            requester=ReleaseBotRequester(name="toor", is_admin=True, permissions=[]),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/30",
            command=ReleaseBotCommand(
                name="build", package="stuff", labels=set(["unstable"])
            ),
            requester=ReleaseBotRequester(
                name="colab", is_admin=False, permissions=["collaborator/stuff"]
            ),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/31",
            command=ReleaseBotCommand(
                name="build", package="stuff", labels=set(["experimental"])
            ),
            requester=ReleaseBotRequester(
                name="dev1dev", is_admin=False, permissions=["member"]
            ),
            repo_store=repo_store,
        ),
        ReleaseBotTask(
            id="devuan/stuff/issues/32",
            command=ReleaseBotCommand(
                name="silly", package="stuff", labels=set(["unstable"])
            ),
            requester=ReleaseBotRequester(name="toor", is_admin=True, permissions=[]),
            repo_store=repo_store,
        ),
    ]


# This emulates communication to gitea by hard-coding the responses.
# Of special interest is /repos/issues/search, which is defined above.
responses = {
    "get": {
        # Hard-coded team_id
        "/orgs/devuan/teams/search": {
            "{'q': 'packages', 'include_desc': False}": ResponseMockup(
                response={"data": [{"id": 42}]}
            )
        },
        # colab is collaborator in devuan/stuff, dev1dev and koala aren't
        "/repos/devuan/stuff/collaborators/colab": {"{}": ResponseMockup(ok=True),},
        "/repos/devuan/stuff/collaborators/dev1dev": {"{}": ResponseMockup(ok=False),},
        "/repos/devuan/stuff/collaborators/koala": {"{}": ResponseMockup(ok=False),},
        "/repos/devuan/stuff/collaborators/toor": {"{}": ResponseMockup(ok=False),},
        "/repos/issues/search": {
            "{'state': 'open', 'q': 'build'}": ResponseMockup(
                ok=False, response=tested_open_issues
            ),
        },
        # dev1dev is member of packages, colab and koala aren't
        "/teams/42/members/dev1dev": {"{}": ResponseMockup(ok=True),},
        "/teams/42/members/colab": {"{}": ResponseMockup(ok=False),},
        "/teams/42/members/koala": {"{}": ResponseMockup(ok=False),},
        "/teams/42/members/toor": {"{}": ResponseMockup(ok=False),},
    },
}


class TestRepoStores(unittest.TestCase):
    def test_gitea_init(self):
        self.maxDiff = None

        rs = GiteaRepoStoreMockup(ConfigMockup(), ArgsMockup(), responses=responses)
        # In our testing this team_id is 42
        self.assertEqual(42, rs.packages_team_id)
        # colab is a collaborator for package "stuff"
        self.assertTrue(rs.is_package_collaborator("colab", "stuff"))
        # koala is not
        self.assertFalse(rs.is_package_collaborator("koala", "stuff"))

        # dev1dev is a member of the packages team
        self.assertTrue(rs.is_packages_member("dev1dev"))
        # colab and koala are not
        for dev in ["colab", "koala"]:
            self.assertFalse(
                rs.is_packages_member(dev),
                "Wrong packages_member status for: {}".format(dev),
            )
        # Open issues: should be verbatim
        open_issues = rs._get_build_issues()
        self.assertEqual(tested_open_issues, open_issues)
        # Pending tasks: should be filtered by namespace / assignee
        pending_tasks = list(rs.get_pending_tasks())
        self.assertEqual(expected_pending_tasks(rs), pending_tasks)


class TestJobStores(unittest.TestCase):
    def test_jenkins_init(self):
        test_job = "test-job"
        job_store = JenkinsJobStoreMockup(
            ConfigMockup(data={"jenkins": {"build_job": test_job}}), ArgsMockup()
        )

        url1 = job_store.queue_build("beowulf-proposed", "releasebot-test")
        url2 = job_store.queue_build("chimaera-proposed", "releasebot-test")
        self.assertEqual("/test-builds/10", url1)
        self.assertEqual("/test-builds/11", url2)
        builds = job_store.jk.data[test_job]["builds"]
        self.assertEqual(2, len(builds))
        self.assertEqual(
            {"codename": "beowulf-proposed", "srcpackage": "releasebot-test"}, builds[0]
        )
        self.assertEqual(
            {"codename": "chimaera-proposed", "srcpackage": "releasebot-test"},
            builds[1],
        )


class TestReleaseBot(unittest.TestCase):
    def get_task_messages(self, task_id: int, repo_store: GiteaRepoStoreMockup):
        return [i[1] for i in repo_store.messages if "/{}/".format(task_id) in i[0]]

    def test_releasebot(self):
        test_job = "test-job"
        config = ConfigMockup(
            data={
                "jenkins": {"build_job": test_job},
                "filters": {
                    "suites": "experimental,unstable",
                    "restricted_suites": "jessie, ascii, beowulf",
                    "restricted_suffixes": "proposed, proposed-updates, proposed-security",
                },
            }
        )
        args = ArgsMockup()

        repo_store = GiteaRepoStoreMockup(config, args, responses=responses)
        job_store = JenkinsJobStoreMockup(config, args)
        releasebot = ReleaseBot(
            config, args, job_store=job_store, repo_store=repo_store
        )

        result = list(releasebot.process())
        self.assertEqual(
            len(expected_pending_tasks(repo_store)),
            len(repo_store.resolved_tasks),
            "Did not resolve all pending tasks",
        )

        # Check that for each task that was pending we did 'the right thing'
        for t in tested_open_issues:
            task_id = t["number"]
            messages = self.get_task_messages(task_id, repo_store)
            # First ensure the relevant messages were sent
            if "expected_messages" in t:
                self.assertEqual(
                    len(t["expected_messages"]),
                    len(messages),
                    "Unexpected message count for task: {}".format(task_id),
                )
                for (expected, message) in zip(t["expected_messages"], messages):
                    self.assertIn(
                        expected,
                        message,
                        "Unexpected message for task: {}".format(task_id),
                    )
            # Now ensure the relevant builds were queued
            for build in t.get("expected_builds", []):
                self.assertIn(build, job_store.jk.data.get(test_job)["builds"])
