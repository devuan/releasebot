import unittest

from tests.lib import ArgsMockup


class TestReleaseBoHelperClasses(unittest.TestCase):
    def test_ReleaseBotCommand(self):
        from releasebot import ReleaseBotCommand

        cmd = ReleaseBotCommand(
            name="Name", package="Package", labels=["cool", "stuff"]
        )
        self.assertEqual("Name", cmd.name)
        self.assertEqual("Package", cmd.package)
        self.assertEqual(["cool", "stuff"], cmd.labels)

    def test_ReleaseBotRequester(self):
        from releasebot import ReleaseBotRequester

        req = ReleaseBotRequester(name="Name", is_admin=True)
        self.assertEqual("Name", req.name)
        self.assertEqual(True, req.is_admin)
        self.assertEqual([], req.permissions)
        req = ReleaseBotRequester(
            name="Name2", is_admin=False, permissions=["contributor/Package"]
        )
        self.assertEqual("Name2", req.name)
        self.assertEqual(False, req.is_admin)
        self.assertEqual(["contributor/Package"], req.permissions)

    def test_ReleaseBotTask(self):
        from releasebot import GenericRepoStore, ReleaseBotTask

        class RS(GenericRepoStore):
            def __init__(self):
                self.args = ArgsMockup()

            def get_pending_tasks(self):
                return []

            def do_notify_task(self, task: ReleaseBotTask, message: str, resolve: bool):
                self._message = message
                self._resolve = resolve

        rs = RS()
        task = ReleaseBotTask(id="test", command=None, requester=None, repo_store=rs)
        message = "Test Message"
        task.notify(message)
        self.assertEqual(message, rs._message)
        self.assertEqual(False, rs._resolve)

        rs = RS()
        task = ReleaseBotTask(id="test2", command=None, requester=None, repo_store=rs)
        message = "Test Message2"
        task.notify(message, resolve=True)
        self.assertEqual(message, rs._message)
        self.assertEqual(True, rs._resolve)
