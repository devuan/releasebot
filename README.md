# Devuan Releasebot

## Dependencies

This depends exclusively on following python 3.5+ libraries:

    apt install python3-requests python3-jenkins python3-attr

For development you may want to use `pipenv install --dev`.

## Testing

This project uses [unittest][unittest] for testing and is still WIP, please
consider adding more test-cases to the tests module.

To run the tests, use:

    python -m unittest

Of special interest are canonical examples of permissions handling and
decision making in ReleaseBot.

[unittest]: https://docs.python.org/3/library/unittest.html

## WARNING: this is beta code written for internal use in Devuan,
do not use it if you don't know what you are doing.

If it will kill your cat, burn your house, cut your fingers or any other damage
the authors will not responsable for anything.

Devuan Releasebot is a little python script to be launched by cron ( please, do not use
systemd-crond for it, only "normal" crond! ) to autobuild devuan packages by 
communicating with both gitea and jenkins.

With the newer architecture it should be plausible to add different repository
sources and build systems like GitLab or buildbot respectively
